

variable "box_file_name" {
  default = env("BOX_FILE_NAME")
}



source "vagrant" "skc_base_debian_10" {
  communicator = "ssh"
  output_dir   = "build"
  source_path  = "debian/buster64"
  provider     = "virtualbox"
  add_force    = true
}

build {
  sources = ["source.vagrant.skc_base_debian_10"]

  
  provisioner "ansible" {
    playbook_file = "provision/ansible/build.yml"
    ansible_env_vars = [ "ANSIBLE_CONFIG=provision/ansible/ansible.cfg" ]
  }

  # post-processor "shell-local" {
  #   inline = ["echo renaming package.box file", "mv build/package.box build/${var.box_file_name}", "echo renaming package.box file is complete"]
  # }

}
